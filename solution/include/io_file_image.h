#ifndef IO_FILE
#define IO_FILE

#include "image.h"
#include "io_status.h"

enum read_status read_image(const char *file_path, struct image *origin_image);

enum write_status write_image(const char *file_path, const struct image *result_image);

#endif

#ifndef IO_STATUS
#define IO_STATUS

#define READ_ERROR_MESSAGES_COUNT 9
#define TRANSFORM_ERROR_MESSAGES_COUNT 2
#define WRITE_ERROR_MESSAGES_COUNT 7

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_MALLOC_FAILED,
    READ_INVALID_FILE,
    READ_FILE_CLOSING_FAILED,
    READ_INVALID_OFFSET,
    READ_INVALID_PADDING
};

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_INVALID_SIGNATURE,
    WRITE_INVALID_HEADER,
    WRITE_INVALID_FILE,
    WRITE_FILE_CLOSING_FAILED,
    WRITE_INVALID_BITS,
    WRITE_INVALID_PADDING
};

/*  transformation   */
enum transform_status  {
    TRANSFORM_OK = 0,
    TRANSFORM_MALLOC_FAILED
};

#endif

#ifndef IO_BMP_IMAGE
#define IO_BMP_IMAGE

#include "image.h"
#include "io_status.h"
#include <stdio.h>

enum read_status from_bmp( FILE* in, struct image* img );

enum write_status to_bmp( FILE* out, struct image const* img );

#endif

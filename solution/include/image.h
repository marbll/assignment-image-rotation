#ifndef IMAGE
#define IMAGE

#include  <stdint.h>

struct __attribute__((__packed__)) pixel { uint8_t b, g, r; };

struct __attribute__((__packed__)) image {
    uint64_t width, height;
    struct pixel* data;
};

struct pixel* allocate(uint32_t width, uint32_t height);

void free_px(struct pixel* px);

void set_pixel(struct image* img, struct pixel px, uint32_t x, uint32_t y);

#endif

#ifndef ROTATE
#define ROTATE

#include "image.h"
#include "io_status.h"

enum transform_status rotate_image(const struct image* img, struct image* out);

#endif

#include "io_bmp_image.h"
#include "io_file_image.h"
#include <stdio.h>

enum read_status read_image(const char *file_path, struct image *origin_image){
    FILE* file = fopen(file_path,"rb");
    if (file == NULL)  return READ_INVALID_FILE;

    enum read_status rstatus = from_bmp(file, origin_image);
    int close_file = fclose(file);
    if (close_file == EOF) {
        if (rstatus == READ_OK) rstatus = READ_FILE_CLOSING_FAILED;
    }

    return rstatus;
}

enum write_status write_image(const char *file_path, const struct image *result_image){
    FILE* file = fopen(file_path,"wb");
    if (file == NULL) return WRITE_INVALID_FILE;

    enum write_status wstatus = to_bmp(file, result_image);
    int close_file = fclose(file);
    if (close_file == EOF) {
        if (wstatus == WRITE_OK) wstatus = WRITE_FILE_CLOSING_FAILED;
    }

    return wstatus;
}

#include "io_file_image.h"
#include "rotate.h"
#include <stdio.h>

char* read_error_messages[READ_ERROR_MESSAGES_COUNT] = {
        [READ_INVALID_SIGNATURE] = "Signature invalid. ",
        [READ_INVALID_BITS] = "Bits invalid. ",
        [READ_INVALID_HEADER] = "Header invalid. ",
        [READ_MALLOC_FAILED] = "Allocation failed. ",
        [READ_INVALID_FILE] = "File opening failed. ",
        [READ_FILE_CLOSING_FAILED] = "File closing failed. ",
        [READ_INVALID_OFFSET] = "Offset invalid. ",
        [READ_INVALID_PADDING] = "Padding invalid. "
};

char* transform_error_messages[TRANSFORM_ERROR_MESSAGES_COUNT] = {
        [TRANSFORM_MALLOC_FAILED] = "Allocation failed. "
};

char* write_error_messages[WRITE_ERROR_MESSAGES_COUNT] = {
        [WRITE_INVALID_SIGNATURE] = "Signature invalid. ",
        [WRITE_INVALID_HEADER] = "Header invalid. ",
        [WRITE_INVALID_FILE] = "File opening failed. ",
        [WRITE_FILE_CLOSING_FAILED] = "File closing failed. ",
        [WRITE_INVALID_BITS] = "Bits invalid. ",
        [WRITE_INVALID_PADDING] = "Padding invalid. "
};

void print_read_error_message(enum read_status rstatus) {
    fprintf(stderr, "%s", read_error_messages[rstatus]);
    fprintf(stderr, "Reading image ERROR\n");
}

void print_transform_error_message(enum transform_status tstatus) {
    fprintf(stderr, "%s", transform_error_messages[tstatus]);
    fprintf(stderr, "Transforming image ERROR\n");
}

void print_write_error_message(enum write_status wstatus) {
    fprintf(stderr, "%s", write_error_messages[wstatus]);
    fprintf(stderr, "Writing image ERROR\n");
}

int main( int argc, char** argv ) {
    struct image source = {0};
    struct image result = {0};
    enum read_status rstatus = READ_OK;
    enum transform_status tstatus = TRANSFORM_OK;
    enum write_status wstatus = WRITE_OK;
    if (argc != 3) {
        fprintf(stderr, "Provide 2 arguments - source image and result image\n");
        return 1;
    }

    rstatus = read_image(argv[1], &source);
    if (rstatus != READ_OK) {
        if (rstatus != READ_MALLOC_FAILED) free_px(source.data);
        print_write_error_message(wstatus);
        return 2;
    }

    tstatus = rotate_image(&source, &result);
    if (tstatus != TRANSFORM_OK) {
        if (tstatus != TRANSFORM_MALLOC_FAILED) free_px(result.data);
        print_transform_error_message(tstatus);
        return 3;
    }

    wstatus = write_image(argv[2], &result);
    free_px(result.data);
    if (wstatus != WRITE_OK) {
        print_write_error_message(wstatus);
        return 4;
    }

    free_px(source.data);
    return 0;
}

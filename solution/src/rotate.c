#include "rotate.h"
#include <stddef.h>

enum transform_status rotate_image(const struct image* img, struct image* out){
    struct pixel* new_image_data = allocate(img->width, img->height);
    if (new_image_data == NULL) return TRANSFORM_MALLOC_FAILED;
    *out = (struct image) {.width = img->height, .height = img->width, .data = new_image_data};
    for (uint32_t i = 0; i < img->height; i++){
        for (uint32_t j = 0; j < img->width; j++){
            set_pixel(out, img->data[i*img->width + j],  (img->height - 1 - i), j);
        }
    }
    return TRANSFORM_OK;
}

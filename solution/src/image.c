#include "image.h"
#include  <malloc.h>

struct pixel* allocate(uint32_t width, uint32_t height){
    return malloc(height * width * sizeof(struct pixel));
}

void free_px(struct pixel *px) {
    free(px);
}

void set_pixel (struct image* img, struct pixel px, uint32_t x, uint32_t y){
    img->data[y*img->width + x] = px;
}

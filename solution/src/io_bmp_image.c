#include "bmp_header.h"
#include "io_bmp_image.h"

#define BMP_SIGNATURE 0x4D42
#define BIT_PER_COLOR 24
#define PIXEL_PER_M 2834
#define PLANES 1
#define BI_SIZE 40

uint32_t get_padding(uint32_t width) {
    return 4 - (width * sizeof(struct pixel))%4;
}

size_t read_header(FILE* in, struct bmp_header* header) {
    return fread(header, sizeof(struct bmp_header), 1, in);
}

enum read_status read_body(FILE* in, struct image* img, size_t padding) {
    size_t body_read;
    int padding_seek;
    for (size_t i = 0; i < img->height; i++){
        body_read = fread(img->data + i*img->width, sizeof(struct pixel), img->width, in);
        if (body_read != img->width) return READ_INVALID_BITS;
        padding_seek = fseek(in, padding, SEEK_CUR);
        if (padding_seek != 0) return READ_INVALID_PADDING;
    }
    return READ_OK;
}

enum read_status from_bmp( FILE* in, struct image* img ){
    struct bmp_header header = {0};

    size_t header_read = read_header(in, &header);
    if (header_read != 1) return READ_INVALID_HEADER;

    struct pixel* image_data = allocate(header.biWidth, header.biHeight);
    if (image_data == NULL) return READ_MALLOC_FAILED;

    *img = (struct image) {.width = header.biWidth, .height = header.biHeight, .data = image_data};
    if ( !(img->width > 0 && img->height > 0) ) return READ_INVALID_SIGNATURE;

    int offset_seek =  fseek(in, header.bOffBits, SEEK_SET);
    if (offset_seek != 0) return READ_INVALID_OFFSET;

    size_t padding = get_padding(img->width);
    return read_body(in, img, padding);
}

struct bmp_header create_header(uint32_t width, uint32_t height) {
    size_t img_size = height * (width + get_padding(width)) * sizeof(struct pixel);
    size_t header_size = sizeof(struct bmp_header);
    return (struct bmp_header) {
            .bfType = BMP_SIGNATURE,
            .biHeight = height,
            .biWidth = width,
            .bOffBits = header_size,
            .biSize = BI_SIZE,
            .biPlanes = PLANES,
            .bfileSize = header_size + img_size,
            .biCompression = 0,
            .bfReserved = 0,
            .biBitCount = BIT_PER_COLOR,
            .biXPelsPerMeter = PIXEL_PER_M,
            .biYPelsPerMeter = PIXEL_PER_M,
            .biClrUsed = 0,
            .biClrImportant = 0,
            .biSizeImage = img_size
    };
}

size_t write_header(FILE* out, struct bmp_header* header) {
    return fwrite(header, sizeof(struct bmp_header), 1, out);
}

enum write_status write_body(FILE* out, struct image const *img, size_t padding) {
    size_t write_body;
    int write_padding;
    for (uint32_t i = 0; i < img->height; i++){
        write_body = fwrite(&img->data[i*img->width], sizeof(struct pixel), img->width, out);
        if (write_body != img->width) return WRITE_INVALID_BITS;
        for (size_t i = 0; i < padding; i++) {
            write_padding = putc( 0, out );
            if (write_padding == EOF) return WRITE_INVALID_PADDING;
        }
    }
    return WRITE_OK;
}

enum write_status to_bmp( FILE *out, struct image const *img ) {
    struct bmp_header header = create_header(img->width, img->height);
    if ( !(header.biHeight > 0 && header.biWidth > 0) ) return WRITE_INVALID_SIGNATURE;

    size_t header_write = write_header(out, &header);
    if (header_write != 1) return WRITE_INVALID_HEADER;

    size_t padding = get_padding(header.biWidth);
    return write_body(out, img, padding);
}
